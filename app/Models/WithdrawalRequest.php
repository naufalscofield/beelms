<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Lms;
use App\User;

class WithdrawalRequest extends Model
{
    protected $table    = 'withdrawal_request';

    public function lms()
    {
        return $this->hasOne(Lms::class, 'code', 'lms_code');
    }

    public function verified()
    {
        return $this->hasOne(User::class, 'id', 'updated_by');
    }
}
