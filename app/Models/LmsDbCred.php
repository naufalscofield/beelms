<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Lms;

class LmsDbCred extends Model
{
    protected $table = "lmsdb_cred";

    public function lms()
    {
        return $this->hasOne(Lms::class, 'id', 'id_lms');
    }
}
