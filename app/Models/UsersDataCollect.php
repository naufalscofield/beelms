<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Lms;

class UsersDataCollect extends Model
{
    protected $table = "users_data_collect";

    public function lms()
    {
        return $this->hasOne(Lms::class, 'id', 'id_lms');
    }
}
