<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Lms;
use App\User;

class CutOff extends Model
{
    protected $table    = 'cut_off';

    public function lms()
    {
        return $this->hasOne(Lms::class, 'id', 'id_lms');
    }

    public function last_update()
    {
        return $this->hasOne(User::class, 'id', 'updated_by');
    }
}
