<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Lms;

class MarketingEmail extends Model
{
    protected $table = "marketing_email";

    public function lms()
    {
        return $this->hasOne(Lms::class, 'code', 'lms_code');
    }
}
