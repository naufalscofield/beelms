<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LmsDbCred;
use App\Models\UsersDataCollect;
use App\Models\Lms;

use App\Helpers\DatabaseConnection;
use Datatables;
use Session;

class UsersDataCollectController extends Controller
{
    public function index()
    {
        $data['title']  = 'Users Data Collection';
        $data['lms']    = Lms::all();
        return view("admin.users_data_collect", $data);
    }

    public function data($id,$user_type)
    {
        $data = UsersDataCollect::where('id_lms', $id)
                        ->where('user_type', $user_type)
                        ->with('lms')
                        ->get();
        if ($data)
        {
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('lms', function($row){
                        return $row['lms']['name'];
                    })
                    ->addColumn('action', function($row){
                        $btn = '<button title="Detail" class="btn btn-user" data-id="'.encrypt($row->id).'">
                                    <i class="fa fa-search"></i>
                                </button>';
                        return $btn;
                    })
                    ->rawColumns(['lms', 'action'])
                    ->make(true);
        }
    }

    public function start()
    {
        $lms = LmsDbCred::with('lms')->orderBy('id','ASC')->get();
        foreach ($lms as $cred) {
            if (!empty($cred->driver) &&  !empty($cred->host) && !empty($cred->username) && !empty($cred->password) && !empty($cred->database) && !empty($cred->lms['code'])) {                
            $connection = DatabaseConnection::setConnection($cred);  
            $max_id = UsersDataCollect::where('id_lms',$cred->id_lms)->max('id_user_lms');
            $users_lms = $connection->table('ls_m_user')->whereNotIn('role',[1]);
            if (!empty($max_id)) {
                $users_lms->where('id', '>', $max_id);
            }
            if ($users_lms->count() > 0) {
                $user = '';
                $user = $users_lms->distinct('email')->get();
                foreach ($user as $value) {
                    $user_collect  = new UsersDataCollect;
                    $user_collect->first_name  = $value->nama_depan;
                    $user_collect->last_name   = $value->nama_belakang;
                    $user_collect->email       = $value->email;
                    $user_collect->user_type   = $value->role == 3 ? 'student' : 'lecturer';
                    $user_collect->id_lms      = $cred->id_lms;
                    $user_collect->id_user_lms = $value->id;

                    $user_collect->phone_number = $value->no_hp;
                    $user_collect->institution  = isset($value->institusi);
                    $user_collect->gender       = $value->jk;

                    try{
                        $user_collect->save();
                        Session::flash('flash_success', 'Success to Collect Data');
                    } catch (\Exception $e) {
                        Session::flash('flash_error', $e->getMessage());
                    }

                }
            }

            }

        }

        return redirect()->back();
    }

    public function show($id)
    {
        $id     = decrypt($id);
        $data   = UsersDataCollect::find($id);
        
        if ($data)
        {
            $html = view("admin.detail_users_data_collect", $data)->render();
            $res    = ['data' => $data, 'msg' => 'success','html' => $html];
            $status = 200; 
        } else
        {
            $res    = ['data' => null, 'msg' => 'faild'];
            $status = 400;
        }

        return response()->json($res, $status);
    }
}
