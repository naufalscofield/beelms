<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Validator;
use App\User;
use App\Models\Lms;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Auth;
use Hash;
use Session;

class AuthController extends Controller
{
    public function token(Request $request)
    {
        $rules  = [
            'api_key'           => 'required',
        ];
    
        $messages = [
            'api_key.required'           => 'API Key Required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
    
        if($validator->fails())
        {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $lms    = Lms::where("api_key", $request->api_key)->first();
        if ($lms != NULL)
        {
            // $user   = User::where('name', '__'.$lms->code)->first();
            $user   = User::where('email', 'admin@beelms.com')->first();
            if (!$userToken = JWTAuth::fromUser($user)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
            $response = [
                'lms_code' => $lms->code,
                'token' => $userToken
            ];

            return response()->json($response,200);
            
        } else
        {
            return response()->json(['error' => 'invalid API key'], 403);
        }
    }

    // public function tokena(Request $request)
    // {
    //     $rules  = [
    //         'api_key'           => 'required',
    //         'id_withdrawal'     => 'required',
    //         'id_lecturer'       => 'required',
    //         'name'              => 'required',
    //         'total'             => 'required',
    //     ];
    
    //     $messages = [
    //         'api_key.required'           => 'API Key Required',
    //         'id_withdrawal.required'     => 'ID Withdrawal Required',
    //         'id_lecturer.required'       => 'ID Lecturer Required',
    //         'name.required'              => 'Lecturer Name Required',
    //         'total.required'             => 'Total Required',
    //     ];
    
    //     $validator = Validator::make($request->all(), $rules, $messages);
    
    //     if($validator->fails())
    //     {
    //         return response()->json($validator->errors()->toJson(), 400);
    //     }

    //     $lms    = Lms::where("api_key", $request->api_key)->first();
    //     if ($lms != NULL)
    //     {
    //         // $user   = User::where('name', '__'.$lms->code)->first();
    //         $user   = User::where('email', 'admin@beelms.com')->first();
    //         if (!$userToken = JWTAuth::fromUser($user)) {
    //             return response()->json(['error' => 'invalid_credentials'], 401);
    //         }

    //         $dataPost = [
    //             'lms_code'          => $lms->code,
    //             'id_withdrawal'     => $request->id_withdrawal,
    //             'id_lecturer'       => $request->id_lecturer,
    //             'name'              => $request->name,
    //             'total'             => $request->total,
    //         ];

    //         $response = Curl::to(asset('').'api/withdrawal-request/store')
    //                         ->withHeader('Authorization: Bearer '.$userToken)
    //                         ->withData($dataPost)
    //                         ->post();
    //         return response()->json($response,200);
            
    //     } else
    //     {
    //         return response()->json(['error' => 'invalid API key'], 401);
    //     }
    // }

    public function index()
    {
        return view("auth.login");
    }

    public function check(Request $request)
    {
        $rules  = [
            'email'     => 'required|email',
            'password'  => 'required|string'
        ];

        $messages = [
            'email.required'        => 'Email is required',
            'email.email'           => 'Email invalid',
            'password.required'     => 'Password is required',
            'password.string'       => 'Password must be a character'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            $error  = $validator->messages()->get('*');
            foreach ($error as $e)
            {
                foreach ($e as $e)
                {
                    toastr()->error($e);
                }
            }
            return redirect()->back()->withInput($request->all());
        }

        $cred = [
            'email'     => $request->input('email'),
            'password'  => $request->input('password'),
        ];
        
        Auth::attempt($cred);

        if (Auth::check()) { 
            Session::flash('flash_success', 'Welcome back '.Auth::user()->name);
            return redirect()->route('admin.withdrawal.request');
        } else {
            toastr()->error("Invalid credentials");
            return redirect()->back()->withInput($request->all());
        }
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        toastr()->success('See you later');
        return redirect()->route('login');
    } 

}
