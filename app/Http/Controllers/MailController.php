<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Mail;
use Log;
use Validator;
use App\Models\Lms;
use App\Models\UsersDataCollect;
use App\Models\MarketingEmail;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Jobs\ProcessSendEmail;
use App\Jobs\ProcessSendEmailMarketing;


class MailController extends Controller {
   public function index() {
     $data['title']  = 'Blast Email';
     $data['lms']    = Lms::all();
     return view("admin.blast_email", $data);
   }

   public function save(Request $request){
         if ($request->id_lms == 'all') {
            $data_email   = UsersDataCollect::select('email','first_name','last_name')->groupBy(['email','first_name','last_name'])->get()->toArray();
         }else{
            $where = [];
            $where['id_lms'] = $request->id_lms;
            if ($request->user_type != 'all') {
               $where['user_type'] = $request->user_type;
            }
            $data_email   = UsersDataCollect::where($where)->get()->toArray();
         }
         $image_url = NULL;
         if ($request->hasFile('image')) {
            $file           = $request->file('image');
            $file_name      = date('d-m-Y').'_'.strtolower(str_replace(' ', '', $request->subject)).'.'.$file->getClientOriginalExtension();

            $upload_path    = public_path() . '/assets/email_images'; 
            $file->move($upload_path, $file_name);

            $image_url = asset('').'public/assets/email_images/'.$file_name;
         }
        
         foreach ($data_email as $value) {
            try {
               $detail_email =  [ 
                  "subject" => $request->subject,
                  "email" => $value['email'],
                  "data" => ["name" => $value['first_name']. ' '. $value['last_name'],"email_message" => $request->email_message, "image" => $image_url]
               ];
               ProcessSendEmail::dispatch($detail_email);

               Session::flash('flash_success', 'Success');
            } catch (\Exception $e) {
               Log::channel('error-blast-email')->info(basename(__FILE__) .' ('. __LINE__.') '.$e->getMessage());
               Session::flash('flash_error','Failed');
            }
         }
     
      return redirect()->back();
   }

   public function pushMarketingEmail(Request $request){
      $list = MarketingEmail::with('lms')->get();

      foreach ($list as $l)
      {
         $data_email   = UsersDataCollect::where(['id_lms'=> $l->lms->id,'user_type'=>$l->target])->get()->toArray();
         foreach ($data_email as $value) {
            try {
               $detail_email =  [ 
                  "subject" => $l->subject,
                  "email" => $value['email'],
                  "data" => ["email_message" => $l->message]
               ];
               ProcessSendEmailMarketing::dispatch($detail_email);
            } catch (\Exception $e) {
               Log::channel('error-blast-email')->info(basename(__FILE__) .' ('. __LINE__.') '.$e->getMessage());
               echo $e->getMessage();
            }
         }

         $me   = MarketingEmail::find($l->id)->delete();
      }
   
   echo 'Success';
   }

   public function storeMarketing(Request $request)
   {
      $rules  = [
         'subject'   => 'required',
         'message'   => 'required',
         'target'    => 'required',
         'product'   => 'required',
         'lms_code'  => 'required'
      ];
 
      $messages = [
            'subject.required'           => 'Subject Required',
            'message.required'           => 'Message Required',
            'target.required'            => 'Target Required',
            'product.required'           => 'Product Required',
            'lms_code.required'          => 'LMS Code Required',
      ];

      $validator = Validator::make($request->all(), $rules, $messages);

      if($validator->fails())
      {
         return response()->json($validator->errors()->toJson(), 400);
      }

      $data   = new MarketingEmail;
      $data->lms_code         = $request->lms_code;
      $data->subject          = $request->subject;
      $data->message          = $request->message;
      $data->product          = $request->product;
      $data->target           = $request->target;

      try{
          $data->save();
      } catch (\Exception $e)
      {
          return response()->json($e->getMessage(), 500);
      }

      return response()->json(['msg' => 'success'], 200);

   }
}