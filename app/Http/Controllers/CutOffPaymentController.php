<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Models\CutOff;
use App\Models\Lms;
use Datatables;
use Auth;
use Session;
use Validator;
use Ixudra\Curl\Facades\Curl;

class CutOffPaymentController extends Controller
{
    public function index()
    {
        $data['lms']    = Lms::all();
        $data['title']  = 'Cut Off Payment';
        return view('admin.cut_off', $data);
    }

    public function data($id)
    {
        $data = CutOff::where('id_lms', $id)
                        ->with('lms')
                        ->with('last_update')
                        ->get();

        if ($data)
        {
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('lms', function($row){
                        return $row['lms']['name'];
                    })
                    ->addColumn('cut', function($row){
                        if ($row['type'] == 'persentase')
                        {
                            return $row['cut'].'%';
                        } else
                        {
                            return "Rp " . number_format($row['cut'],2,',','.');

                        }
                        return $row['lms']['name'];
                    })
                    ->addColumn('updated_by', function($row){
                        return $row['last_update']['name'];
                    })
                    ->addColumn('action', function($row){
                        $btn = '<button title="Edit" class="btn btn-info btn-edit" data-id="'.encrypt($row->id).'">
                                    <i class="fa fa-pencil"></i>
                                </button>';
                        return $btn;
                    })
                    ->rawColumns(['lms', 'action'])
                    ->make(true);
        }
    }

    public function show($id)
    {
        $id     = decrypt($id);
        $data   = CutOff::find($id);

        if($data)
        {
            $ids = [
                'id'        => encrypt($data->id),
                'id_lms'    => encrypt($data->id_lms),
            ];

            return response()->json(['data' => $data, 'ids' => $ids, 'msg' => 'success'],200);
        } else
        {
            return response()->json(['data' => NULL, 'msg' => 'Something went wrong, failed to get requested data'],500);
        }
    }

    public function update(Request $request)
    {
        $rules  = [
            'edit_cut'  => 'required|numeric|min:0|max:99.99',
        ];
    
        $messages = [
            'edit_cut.required' => 'Cut Fee Required',
            'edit_cut.min'      => 'Cut Fee Minimal 0',
            'edit_cut.max'      => 'Cut Fee Maximal 99.99',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        // dd($validator->errors()->first());
        if($validator->fails())
        {
            Session::flash('flash_error', $validator->errors()->first());
            return redirect()->back();
        }
        
        $id     = decrypt($request->edit_id);
        $id_lms = decrypt($request->edit_id_lms);
        $data               = CutOff::find($id);
        $data->description  = $request->edit_description;
        $data->code         = $request->edit_code;
        $data->cut          = $request->edit_cut;
        $data->type         = $request->edit_type;
        $data->updated_by   = Auth::user()->id;

        $lms                = Lms::find($id_lms);
        $send               = [
            'api_key'       => $lms->api_key, 
            'keterangan'    => $request->edit_description,
            'kode'          => $request->edit_code,
            'potongan'      => $request->edit_cut,
            'jenis'         => $request->edit_type
        ];

        $response           = Curl::to($lms->url.'/api/callback-cut-off')
                                    ->withData($send)
                                    ->post();

        $response           = json_decode($response);
        if ($response->status_code == 200)
        {
            try {
                $data->save();
            } catch (\Exception $e)
            {
                Session::flash('flash_error', $e->getMessage());
                return redirect()->back();
            }

            Session::flash('flash_success', $response->message);
            return redirect()->back();
        } else {
            Session::flash('flash_error', $response->message);
            return redirect()->back();
        }
    }
    
}
