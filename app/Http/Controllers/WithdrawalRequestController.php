<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\WithdrawalRequest;
use App\Models\Lms;
use App\User;
use Illuminate\Support\Facades\Crypt;
use Validator;
use Datatables;
use Session;
use Auth;
use Ixudra\Curl\Facades\Curl;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Contracts\JWTSubject;

class WithdrawalRequestController extends Controller
{
    public function index()
    {
        $data['title'] = 'Withdrawal Request';
        return view("admin.withdrawal_request", $data);
    }

    public function store(Request $request)
    {
        $rules  = [
            'lms_code'          => 'required',
            'id_withdrawal'     => 'required',
            'id_lecturer'       => 'required',
            'name'              => 'required',
            'total'             => 'required',
            'rekening_bank'         => 'required',
            'no_rekening'       => 'required',
            'pemilik_rekening'  => 'required',
        ];

        $messages = [
            'lms_code.required'          => 'LMS Code Required',
            'id_withdrawal.required'     => 'ID Withdrawal Required',
            'id_lecturer.required'       => 'ID Lecturer Required',
            'name.required'              => 'Name Lecturer required',
            'total.required'             => 'Total required',
            'rekening_bank.required'         => 'Nama Bank required',
            'no_rekening.required'       => 'No Rekening Bank required',
            'pemilik_rekening.required'  => 'Nama Pemilik Rekening Bank required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $data   = new WithdrawalRequest;
        $data->lms_code         = $request->lms_code;
        $data->id_withdrawal    = $request->id_withdrawal;
        $data->id_lecturer      = $request->id_lecturer;
        $data->name             = $request->name;
        $data->total            = $request->total;
        $data->type             = $request->type;
        $data->rekening_bank    = $request->rekening_bank;
        $data->no_rekening      = $request->no_rekening;
        $data->pemilik_rekening = $request->pemilik_rekening;
        $data->status           = 2;
        $data->receipt          = NULL;
        $data->created_at       = date("Y-m-d H:i:s");

        try{
            $data->save();
        } catch (\Exception $e)
        {
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(['msg' => 'success'], 200);
    }

    public function data($status)
    {
        $data   = WithdrawalRequest::where('status', $status)
                ->with('lms')    
                ->with('verified')    
                ->get();
                
                if ($status == 2)
                {
                    return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('lms', function($row){
                        return $row['lms']->name;
                    })
                    ->addColumn('amount', function($row){
                        return "Rp " . number_format($row->total,2,',','.');
                    })
                    ->addColumn('action', function($row){
                        $btn = '<button title="Done" class="btn btn-success btn-verify" data-id="'.encrypt($row->id).'" data-code="'.$row->lms_code.'">
                                    <i class="fa fa-check-circle-o"></i>
                                </button>';
                        return $btn;
                    })
                    ->addColumn('created', function($row){
                        $date = 
                        \Carbon\Carbon::parse($row->created_at)->format('d F Y');
                        return $date;
                    })
                    ->addColumn('type', function($row){
                        $type = ($row->type == 'course') ? '<span class="span badge-success">COURSE</span>' : '<span class="span badge-danger">DONASI</span>';
                        return $type;
                    })
                    ->rawColumns(['lms', 'action', 'created', 'amount', 'type'])
                    ->make(true);
                } else if ($status == 3)
                {
                    return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('lms', function($row){
                        return $row['lms']->name;
                    })
                    ->addColumn('verified', function($row){
                        return $row['verified']->name;
                    })
                    ->addColumn('amount', function($row){
                        return "Rp " . number_format($row->total,2,',','.');
                    })
                    ->addColumn('created', function($row){
                        $date = 
                        \Carbon\Carbon::parse($row->created_at)->format('d F Y');
                        return $date;
                    })
                    ->addColumn('type', function($row){
                        $type = ($row->type == 'course') ? '<span class="span badge-success">COURSE</span>' : '<span class="span badge-danger">DONASI</span>';
                        return $type;
                    })
                    ->addColumn('action', function($row){
                        $btn = '<button title="Receipt" class="btn btn-success btn-receipt" data-id="'.encrypt($row->id).'">
                                <i class="fa fa-file-text-o"></i>
                                </button>';
                        return $btn;
                    })
                    ->rawColumns(['lms', 'action', 'verified', 'amount', 'created', 'type'])
                    ->make(true);
                }

    }

    public function show($id)
    {
        $id     = decrypt($id);
        $data   = WithdrawalRequest::find($id);
        
        if ($data)
        {
            $res    = ['data' => $data, 'msg' => 'success'];
            $status = 200; 
        } else
        {
            $res    = ['data' => null, 'msg' => 'faild'];
            $status = 400;
        }

        return response()->json($res, $status);
    }

    public function verify(Request $request)
    {
        $id             = decrypt($request->id);
        $validator      = Validator::make($request->all(),[
            'receipt' => 'required'
        ]);

        if ($validator->fails()) {
            Session::flash('flash_error', 'Please complete all required fields!');
            return redirect()->route('admin.withdrawal.request');
        }

        $data           =  WithdrawalRequest::find($id);
        $file           = $request->file('receipt');
        $file_name      = $data->id_withdrawal.'_'.date('d-m-Y').'_'.$request->code.'.'.$file->getClientOriginalExtension();

        $upload_path    = public_path() . '/assets/withdrawal_receipt'; 
        $file->move($upload_path, $file_name);

        // Update status to LMS Public
        $lms        = Lms::where("code", $request->code)->first();
        if ($data->type == 'donasi')
        {
            $end    = '/api/callback_payment_donation';
        } else
        {
            $end    = '/api/callback-payment';
        }
        
        $response   = Curl::to($lms->url.$end)
                            ->withData(['api_key' => $lms->api_key, 'id_withdrawal' => $data->id_withdrawal, 'file_name' => $file_name])
                            ->withFile('file', 'public/assets/withdrawal_receipt/'.$file_name, 'image/png', $file_name)
                            ->post();
        $response   = json_decode($response);
        //

        if ($response->status_code == 200)
        {
            //Update status to BeeLMS
            $data->receipt    = $file_name;
            $data->status     = 3;
            $data->updated_by = (Auth::check()) ? Auth::user()->id : 1; 
            
            try
            {
                $data->save();
                Session::flash('flash_success', 'Payment withdrawal verified');
            } catch (\Exception $e) {
                Session::flash('flash_error', $e->getMessage());
            }
    
            return redirect()->route('admin.withdrawal.request');
        } else
        {
            Session::flash('flash_error', $response->message);
            return redirect()->route('admin.withdrawal.request');
        }

    }
}
