<?php

namespace App\Helpers;
use Config;
use DB;
use App\Helpers\DatabaseConnection;

class DatabaseConnection
{
    public static function setConnection($params)
    {
        config(['database.connections.'.$params->lms['code'] => [
            'driver' => $params->driver,
            'host' => $params->host,
            'username' => $params->username,
            'password' => $params->password,
            'database' => $params->database
        ]]);

        return DB::connection($params->lms['code']);
    }
}