<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'AuthController@index')->name('login');
Route::post('/login/check', 'AuthController@check')->name('login.check');
// Route::post('api/auth/token', 'AuthController@token')->name('auth.token');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'DashboardController@index')->name('admin.dashboard');
    
    Route::get('/logout', 'AuthController@logout')->name('logout');

    Route::get('/withdrawal-request', 'WithdrawalRequestController@index')->name('admin.withdrawal.request');
    Route::get('/withdrawal-request/data/{status}', 'WithdrawalRequestController@data')->name('admin.withdrawal.request.data');
    Route::get('/withdrawal-request/show/{id}', 'WithdrawalRequestController@show')->name('admin.withdrawal.request.show');
    Route::post('/withdrawal-request/verify', 'WithdrawalRequestController@verify')->name('admin.withdrawal.request.verify');

    Route::get('/cut-off-payment', 'CutOffPaymentController@index')->name('admin.cutoff.payment');
    Route::get('/cut-off-payment/data/{id}', 'CutOffPaymentController@data')->name('admin.cutoff.payment.data');
    Route::get('/cut-off-payment/show/{id}', 'CutOffPaymentController@show')->name('admin.cutoff.payment.show');
    Route::post('/cut-off-payment/update', 'CutOffPaymentController@update')->name('admin.cutoff.payment.update');

    ## USER DATA COLLECT
    Route::get('/users-data-collect/show{id}', 'UsersDataCollectController@show')->name('users.data.collect.show');
    Route::get('/users-data-collect/start', 'UsersDataCollectController@start')->name('users.data.collect.start');
    Route::get('/users-data-collect', 'UsersDataCollectController@index')->name('users.data.collect');
    Route::get('/users-data-collect/data/{id}/{user_type}', 'UsersDataCollectController@data')->name('users.data.collect.data');

    Route::get('/blast-email','MailController@index')->name('admin.blast.email');
    Route::get('/push-marketing-email','MailController@pushMarketingEmail')->name('admin.push.markekting.email');
    Route::post('/blast-email/save','MailController@save')->name('admin.blast.email.save');
    Route::get('/sendhtmlemail','MailController@html_email');
    Route::get('/viewemail','MailController@view_email');

});
