<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/auth/token', 'AuthController@token')->name('auth.token');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::post('/withdrawal-request', 'WithdrawalRequestController@store')->name('admin.withdrawal_request.store');
    Route::post('/email-marketing', 'MailController@storeMarketing')->name('admin.email.marketing.store');
});

