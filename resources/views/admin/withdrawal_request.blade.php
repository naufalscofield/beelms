@extends('admin.template', ['title' => $title])

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-danger">
              <h4 class="card-title ">Waiting Request</h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table" id="table_request">
                  <thead class=" text-primary">
                    <th>
                      No
                    </th>
                    <th>
                      LMS
                    </th>
                    <th>
                      ID Withdrawal
                    </th>
                    <th>
                      Name
                    </th>
                    <th>
                      Total
                    </th>
                    <th>
                      Request Date
                    </th>
                    <th>
                      Type Request
                    </th>
                    <th>
                        <i class="fa fa-cogs"></i>
                    </th>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="card card-plain">
            <div class="card-header card-header-success">
              <h4 class="card-title mt-0"> History Request</h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table" id="table_history">
                    <thead class=" text-primary">
                      <th>
                        No
                      </th>
                      <th>
                        LMS
                      </th>
                      <th>
                        ID Withdrawal
                      </th>
                      <th>
                        Name
                      </th>
                      <th>
                        Total
                      </th>
                      <th>
                        Request Date
                      </th>
                      <th>
                        Type Request
                      </th>
                      <th>
                        Verified By
                      </th>
                      <th>
                        <i class="fa fa-cogs"></i>
                      </th>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="modal_verify" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Payment Verification</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="{{ route('admin.withdrawal.request.verify') }}" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}
                <input type="hidden" name="id" id="verify_id" class="form-control">
                <input type="hidden" name="code" id="verify_code" class="form-control">

                <table class="table">
                  <tr>
                    <th>Nama Bank</th>
                    <th id="t_nama_bank"></th>
                  </tr>
                  <tr>
                    <th>No Rekening Bank</th>
                    <th id="t_no_rekening"></th>
                  </tr>
                  <tr>
                    <th>Nama Pemilik Rekening</th>
                    <th id="t_pemilik_rekening"></th>
                  </tr>
                </table>
                
                <label for=""> <span style="color:red">*</span> Payment Receipt</label><br>
                <input type="file" required name="receipt" class="form-control" placeholder="Receipt"/>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Verify</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal_receipt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Payment Receipt</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="text-center">
            <img src="" id="receipt_receipt" alt="">
          </div>
      </div>
    </div>
  </div>


  
  <script>
      $(document).ready(function(){
        var flash_error   = "{{ (Session('flash_error')) }}"
        var flash_success = "{{ (Session('flash_success')) }}"
        var flash_warning = "{{ (Session('flash_warning')) }}"
        var flash_info    = "{{ (Session('flash_info')) }}"

        if (flash_error)
        {
          md.showNotification('top','right', 'danger', flash_error)
        }
        if (flash_success)
        {
          md.showNotification('top','right', 'success', flash_success)
        }
        if (flash_warning)
        {
          md.showNotification('top','right', 'warning', flash_warning)
        }
        if (flash_info)
        {
          md.showNotification('top','right', 'info', flash_info)
        }

        var route1 = "{{ route('admin.withdrawal.request.data', 2) }}"
        var table1 = $('#table_request').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: route1,
            columns: [
                {data: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'lms', name: 'lms'},
                {data: 'id_withdrawal', name: 'id_withdrawal'},
                {data: 'name', name: 'name'},
                {data: 'amount', name: 'amount'},
                {data: 'created', name: 'created'},
                {data: 'type', name: 'type'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
        });

        var route2 = "{{ route('admin.withdrawal.request.data', 3) }}"
        var table2 = $('#table_history').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: route2,
            columns: [
                {data: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'lms', name: 'lms'},
                {data: 'id_withdrawal', name: 'id_withdrawal'},
                {data: 'name', name: 'name'},
                {data: 'amount', name: 'amount'},
                {data: 'created', name: 'created'},
                {data: 'type', name: 'type'},
                {data: 'verified', name: 'verified'},
                {data: 'action', name: 'action'},
            ],
        });

        $(document).on("click", ".btn-verify", function(){
          var id = $(this).data("id")
          var code = $(this).data("code")

          var route = "{{ route('admin.withdrawal.request.show', ":id") }}"
          route     = route.replace(":id", id);
          
          $.get(route, function(data, status){
            $("#t_nama_bank").text(": "+data.data.rekening_bank)
            $("#t_no_rekening").text(": "+data.data.no_rekening)
            $("#t_pemilik_rekening").text(": "+data.data.pemilik_rekening)
            $("#verify_id").val(id)
            $("#verify_code").val(code)
            $("#modal_verify").modal("show")
          }).catch(function (xhr, ajaxOptions, thrownError) {
              md.showNotification('top','right', 'danger', xhr.responseText)
          })

        })

        $(document).on("click", ".btn-receipt", function(){
          var id    = $(this).data("id")
          var route = "{{ route('admin.withdrawal.request.show', ":id") }}"
          route     = route.replace(":id", id);
          
          $.get(route, function(data, status){
            $("#modal_receipt").modal("show")
            $("#receipt_receipt").attr("src", "{{ asset('') }}public/assets/withdrawal_receipt/"+data.data.receipt)
          }).catch(function (xhr, ajaxOptions, thrownError) {
              md.showNotification('top','right', 'danger', xhr.responseText)
          })
        })
      })
  </script>
@endsection