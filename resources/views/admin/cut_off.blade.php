@extends('admin.template', ['title' => $title])

@section('content')
<style>
    .loader {
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 80px;
    height: 80px;
    animation: spin 2s linear infinite;
    }

    @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
    }
</style>
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
                <div class="ml-3" style="color:white">
                    <label style="color:white" for="">Select LMS</label>
                    <select name="" class="form-control" style="width:30%" id="select_id_lms">
                        @foreach ($lms as $l)
                            <option value="{{ $l->id }}">{{ $l->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="card-body">
                <br>
                <br>
                <div class="text-center">
                    <center>

                        <div class="loader" id="div_loading" style="display:none" role="status">
                        </div>
                    </center>
                </div>
                <div class="table-responsive" id="div_responsive" style="display:block">
                <table class="table" id="table">
                  <thead class=" text-primary">
                    <th>
                      No
                    </th>
                    <th>
                      LMS
                    </th>
                    <th>
                      Description
                    </th>
                    <th>
                      Code
                    </th>
                    <th>
                      Cut
                    </th>
                    <th>
                      Last Update By
                    </th>
                    <th>
                        <i class="fa fa-cogs"></i>
                    </th>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit Cut Off</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="{{ route('admin.cutoff.payment.update') }}" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}
                <input type="hidden" name="edit_id" id="edit_id" class="form-control">
                <input type="hidden" name="edit_id_lms" id="edit_id_lms" class="form-control">
                <input type="hidden" name="edit_code" id="edit_code" class="form-control">
                
                <label for=""> <span style="color:red">*</span> Description</label><br>
                <input required type="text" name="edit_description" id="edit_description" class="form-control" placeholder="Description, eg: Biaya admin bank"/><br>
                
                <label for=""> <span style="color:red">*</span> Cut</label><br>
                <input required type="text" name="edit_cut" id="edit_cut" class="form-control" placeholder="Cut fee, eg: 50000"/><br>
                
                <label for=""> <span style="color:red">*</span> Type</label><br>
                <select class="form-control" required name="edit_type" id="edit_type">
                    <option value="">--Select--</option>
                    <option value="persentase">Persentase</option>
                    <option value="nominal">Nominal</option>
                </select>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Verify</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


  
  <script>
      function getTable()
      {
        $('#table').DataTable().destroy()
        $("#table").empty()
        $("#div_responsive").hide()
        $("#div_loading").show()
        var id_lms  = $("#select_id_lms").val()
        var route1  = "{{ route('admin.cutoff.payment.data', ":id_lms") }}"
        route1      = route1.replace(":id_lms", id_lms)
        var table1 = $('#table').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: route1,
            columns: [
                {data: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'lms', name: 'lms'},
                {data: 'description', name: 'description'},
                {data: 'code', name: 'code'},
                {data: 'cut', name: 'cut'},
                {data: 'updated_by', name: 'updated_by'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
        });
        $("#div_loading").hide()
        $("#div_responsive").show()
      }

      $(document).on('change', '#select_id_lms', function(){
        getTable()
      })

      $(document).ready(function(){
        getTable()

        var flash_error   = "{{ (Session('flash_error')) }}"
        var flash_success = "{{ (Session('flash_success')) }}"
        var flash_warning = "{{ (Session('flash_warning')) }}"
        var flash_info    = "{{ (Session('flash_info')) }}"

        if (flash_error)
        {
          md.showNotification('top','right', 'danger', flash_error)
        }
        if (flash_success)
        {
          md.showNotification('top','right', 'success', flash_success)
        }
        if (flash_warning)
        {
          md.showNotification('top','right', 'warning', flash_warning)
        }
        if (flash_info)
        {
          md.showNotification('top','right', 'info', flash_info)
        }

        $(document).on("click", ".btn-edit", function(){
          var id    = $(this).data("id")
          var route = "{{ route('admin.cutoff.payment.show', ":id") }}"
          route     = route.replace(":id", id);
          
          $.get(route, function(data, status){
            $("#modal_edit").modal("show")
            $("#edit_id").val(data.ids.id)
            $("#edit_id_lms").val(data.ids.id_lms)
            $("#edit_description").val(data.data.description)
            $("#edit_code").val(data.data.code)
            $("#edit_cut").val(data.data.cut)
            $("#edit_type").val(data.data.type)

          }).catch(function (xhr, ajaxOptions, thrownError) {
              md.showNotification('top','right', 'danger', xhr.responseText)
            })
        })
      })
  </script>
@endsection