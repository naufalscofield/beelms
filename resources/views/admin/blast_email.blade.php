@extends('admin.template')

@section('content')
<div class="content">
	<div class="container-fluid">
		<div class="row">
	        <div class="col-md-12">
	          <div class="card">
            <form action="{{ route('admin.blast.email.save') }}" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}
	          	<div class="card-header card-header-primary">
                <div class="ml-3" style="color:white">
                    <label style="color:white" for="">Select LMS</label>
                    <select name="id_lms" class="form-control" style="width:30%" id="select_id_lms">
                            <option value="all">All LMS</option>
                        @foreach ($lms as $l)
                            <option value="{{ $l->id }}">{{ $l->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="ml-3" style="color:white">
                    <label style="color:white" for="">User Type</label>
                    <select name="user_type" class="form-control" style="width:30%" id="select_user_type">
                            <option value="all">All User</option>
                            <option value="lecturer">Lecturer</option>
                            <option value="student">Student</option>
                    </select>
                </div>
            </div>

	            <div class="card-body">
                <div class="col-md-12">
                  <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating pl-2">Subject</label>
                    <input type="text" class="form-control" name="subject">
                  </div>
                </div>
                <div class="col-md-12">
                    <label class="bmd-label-floating pl-2">Image</label><br>
                    <input type="file" name="image" class="" placeholder="Image"/>
                </div>
                 <div class="col-md-12">
                    <div class="form-group">
                      <label class="pl-2">Email Message</label>
                      <div class="form-group">
                        {{-- <label class="bmd-label-floating pl-2"> Input your email message to user here ....</label> --}}
                        <textarea id="editor" name="email_message" ></textarea>
                      </div>
                    </div>
                    <div class="ml-3 pull-right" style="color:white">
                      <input type="submit" name="action" value="Send Email" class="btn btn btn-warning btn-submit" />
                    </div>
                  </div>
	            </div>
            </form>
	          </div>
	        </div>
	    </div>
	</div>
</div>
<script>
    $(document).ready(function(){
      $('#editor').summernote({
        options: {
            width: null,
            height: null,

            focus: false,
            tabSize: 4,
          }
      });

      var flash_error   = "{{ (Session('flash_error')) }}"
      var flash_success = "{{ (Session('flash_success')) }}"
      var flash_warning = "{{ (Session('flash_warning')) }}"
      var flash_info    = "{{ (Session('flash_info')) }}"

      if (flash_error)
      {
        md.showNotification('top','right', 'danger', flash_error)
      }
      if (flash_success)
      {
        md.showNotification('top','right', 'success', flash_success)
      }
      if (flash_warning)
      {
        md.showNotification('top','right', 'warning', flash_warning)
      }
      if (flash_info)
      {
        md.showNotification('top','right', 'info', flash_info)
      }

      
    })
</script>
@endsection
