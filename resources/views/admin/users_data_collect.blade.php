@extends('admin.template')

@section('content')
<div class="content">
	<div class="container-fluid">
		<div class="row">
	        <div class="col-md-12">
	          <div class="card">
	          	<div class="card-header card-header-primary">
                <div class="ml-3 pull-right" style="color:white">
                  <br>
                  <a href="{{ route('users.data.collect.start') }}" class=" btn btn-warning"> Collect All Data</a>
                </div>
                <div class="ml-3" style="color:white">
                    <label style="color:white" for="">Select LMS</label>
                    <select name="" class="form-control" style="width:30%" id="select_id_lms">
                        @foreach ($lms as $l)
                            <option value="{{ $l->id }}">{{ $l->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="ml-3" style="color:white">
                    <label style="color:white" for="">User Type</label>
                    <select name="" class="form-control" style="width:30%" id="select_user_type">
                            <option value="lecturer">Lecturer</option>
                            <option value="student">Student</option>
                    </select>
                </div>
            </div>

	            <div class="card-body">
	              <div class="table-responsive">
	                <table class="table" id="table">
	                  <thead class=" text-primary">
	                    <th>
	                      No
	                    </th>
	                    <th>
	                      LMS
	                    </th>
	                    <th>
	                      First-name
	                    </th>
	                    <th>
	                      Last-name
	                    </th>
	                    <th>
	                      User-type
	                    </th>
	                    <th>
	                      Email
	                    </th>
	                    <th>
	                      Institution
	                    </th>
	                    <th>
	                        <i class="fa fa-cogs"></i>
	                    </th>
	                  </thead>
	                  <tbody>
	                  </tbody>
	                </table>
	              </div>
	            </div>
	          </div>
	        </div>
	    </div>
	</div>
</div>
<div class="modal fade" id="modal_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Users Detail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="detail_user"> </div>
    </div>
  </div>
</div>
</div>
<script>
	function getTable()
    {
        $('#table').DataTable().destroy()
        $("#div_responsive").hide()
        $("#div_loading").show()
        var id_lms     =  $("#select_id_lms").val()
        var user_type  =  $("#select_user_type").val()
        var route1  = "{{ route('users.data.collect.data', [":id_lms",":user_type"]) }}"
        route1      = route1.replace(":id_lms", id_lms)
        route2      = route1.replace(":user_type", user_type)
        var table1 = $('#table').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: route2,
            columns: [
                {data: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'lms', name: 'LMS'},
                {data: 'first_name', name: 'First-name'},
                {data: 'last_name', name: 'Last-name'},
                {data: 'user_type', name: 'User-type'},
                {data: 'email', name: 'Email'},
                {data: 'institution', name: 'Institution'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
        });
        $("#div_loading").hide()
        $("#div_responsive").show()
      }

      $(document).on('change', '#select_id_lms', function(){
        getTable()
      })
      $(document).on('change', '#select_user_type', function(){
        getTable()
      })

      $(document).ready(function(){
        getTable()

        var flash_error   = "{{ (Session('flash_error')) }}"
        var flash_success = "{{ (Session('flash_success')) }}"
        var flash_warning = "{{ (Session('flash_warning')) }}"
        var flash_info    = "{{ (Session('flash_info')) }}"

        if (flash_error)
        {
          md.showNotification('top','right', 'danger', flash_error)
        }
        if (flash_success)
        {
          md.showNotification('top','right', 'success', flash_success)
        }
        if (flash_warning)
        {
          md.showNotification('top','right', 'warning', flash_warning)
        }
        if (flash_info)
        {
          md.showNotification('top','right', 'info', flash_info)
        }

        $(document).on("click", ".btn-user", function(){
          var id    = $(this).data("id")
          var route = "{{ route('users.data.collect.show', ":id") }}"
          route     = route.replace(":id", id);
          
          $.get(route, function(data, status){
            $("#modal_user").modal("show")
            $("#detail_user").empty()
            $("#detail_user").append(data.html)
          }).catch(function (xhr, ajaxOptions, thrownError) {
              md.showNotification('top','right', 'danger', xhr.responseText)
            })
        })
      })
</script>
@endsection
