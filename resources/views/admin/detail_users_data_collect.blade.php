<table class="table table-striped table-hover">
	<thead>
		<tr>
			<td width="20%">Name</td>
			<td width="80%">{{ $first_name }} {{ $last_name }}</td>
		</tr>
		<tr>
			<td width="20%">User Type</td>
			<td width="80%">{{ $user_type }}</td>
		</tr>
		<tr>
			<td width="20%">Email</td>
			<td width="80%">{{ $email }}</td>
		</tr>
		<tr>
			<td width="20%">Phone Number</td>
			<td width="80%">{{ $phone_number }}</td>
		</tr>
		<tr>
			<td width="20%">Institution</td>
			<td width="80%">{{ $institution }}</td>
		</tr>
		<tr>
			<td width="20%">Gender</td>
			<td width="80%">{{ $gender == 'P' ? "PEREMPUAN" : "LAKI LAKI" }}</td>
		</tr>
	</thead>
</table>