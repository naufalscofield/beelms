<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login {{ env('APP_NAME') }}</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	{{-- <link rel="icon" type="image/png" href="{{ asset('') }}public/assets/bootstrap/login_v12/images/icons/favicon.ico"/> --}}
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('') }}public/assets/bootstrap/login_v12/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('') }}public/assets/bootstrap/login_v12/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('') }}public/assets/bootstrap/login_v12/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('') }}public/assets/bootstrap/login_v12/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ asset('') }}public/assets/bootstrap/login_v12/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('') }}public/assets/bootstrap/login_v12/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('') }}public/assets/bootstrap/login_v12/css/util.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('') }}public/assets/bootstrap/login_v12/css/main.css">
	<link rel="icon" href="{{ asset('') }}public/assets/img/icon.png">

<!--===============================================================================================-->
@toastr_css
@jquery
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('{{ asset('') }}public/assets/bootstrap/login_v12/images/img-01.jpg');">
			<div class="wrap-login100 p-t-190 p-b-30">
				<form class="login100-form validate-form" action="{{ route('login.check') }}" method="POST">
                    {{ csrf_field() }}
					<div class="">
						<center>
                            <img src="{{ asset('public/assets/img/logo.png') }}" style="width:50%;height:100%" alt="AVATAR"><br>
                        </center>
					</div>

                    <div class="text-center w-full p-t-5 p-b-50">
					</div>

					<div class="wrap-input100 validate-input m-b-10" data-validate = "Email is required">
						<input class="input100" type="email" name="email" placeholder="Email">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock"></i>
						</span>
					</div>

					<div class="container-login100-form-btn p-t-10">
						<button class="login100-form-btn" type="submit">
							Login
						</button>
					</div>

					<div class="text-center w-full p-b-100">
						{{-- <a href="{{ asset('') }}public/assets/bootstraps/login_v1/#" class="txt1">
							Forgot Username / Password?
						</a> --}}
					</div>

					<div class="text-center w-full">
                        <label for="" style="color:white"> Powered by : </label> <img src="{{ asset('') }}public/assets/img/solmit.png" style="width:10%;width:10%" alt="">
					</div>
				</form>
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<script src="{{ asset('') }}public/assets/bootstrap/login_v12/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ asset('') }}public/assets/bootstrap/login_v12/vendor/bootstrap/js/popper.js"></script>
	<script src="{{ asset('') }}public/assets/bootstrap/login_v12/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ asset('') }}public/assets/bootstrap/login_v12/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ asset('') }}public/assets/bootstrap/login_v12/js/main.js"></script>
    @toastr_js
    @toastr_render
</body>
</html>