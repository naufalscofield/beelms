<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLmsdbCredTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lmsdb_cred', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('driver');
            $table->string('host');
            $table->string('username');
            $table->string('password');
            $table->string('database');
            $table->string('id_lms');
            $table->timestamps();
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lmsdb_cred');
    }
}
