<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawalRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdrawal_request', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lms_code');
            $table->integer('id_withdrawal');
            $table->integer('id_lecturer');
            $table->string('name');
            $table->integer('total');
            $table->integer('status');
            $table->string('receipt')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdrawal_request');
    }
}
